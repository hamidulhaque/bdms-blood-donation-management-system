<x-backend.layouts.master>
    
    <div class="container-fluid px-4">
        <h1 class="mt-4">Deleted events</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">
                Dashboard
            </li>
            <li class="breadcrumb-item active">
                Events
            </li>
            <li class="breadcrumb-item active">    
                Trash
            </li>
        </ol>
      
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Deleted Events
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Added by</th>
                            <th>Deleted by</th>
                            <th>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach ($events as $event)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$event->title}}</td>
                            
                            <td>{{$event->addedBy->name}}</td>
                            <td>{{$event->deletedBy->name}}</td>

                            <td class="d-flex"> 
                                
                                <a href="{{route('event.restore',$event->id)}}" title="restore" class="btn btn-info btn-sm" style="color: white" ><i class="fa-solid fa-trash-restore"></i></a> 
                                <form action="{{route('event.delete',$event->id)}}">
                                    <button type="submit"  title="delete" class="btn btn-danger btn-sm" style="margin-left: 3px"><i class="fas fa-trash"></i></button>
                                </form> </td>
                        </tr> 
                        @endforeach
                       
                     
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</x-backend.layouts.master>