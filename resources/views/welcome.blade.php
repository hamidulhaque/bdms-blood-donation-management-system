<x-frontend.layouts.master>

    <style>
        .how-section1 {
            margin-top: -15%;
            padding: 10%;
        }

        .how-section1 h4 {
            color: #ffa500;
            font-weight: bold;
            font-size: 30px;
        }

        .how-section1 .subheading {
            color: #3931af;
            font-size: 20px;
        }

        .how-section1 .row {
            margin-top: 10%;
        }

        .how-img {
            text-align: center;
        }

        .how-img img {
            width: 40%;
        }

        .font-white {
            color: white;
        }
    </style>


    <main>

        <!-- carousel div -->
        <div id="carouselExampleDark" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active font-white"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="10000">
                    <img src="{{ asset('images/carsoul1.jpg') }}" class="d-block w-100" height="800px" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <p class="font-white">A single drop of blood can make a huge difference.</p>
                    </div>
                </div>
                <div class="carousel-item" data-bs-interval="2000">
                    <img src="{{ asset('images/carsoul2.jpg') }}" class="d-block w-100"height="800px" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <p class="font-white">Saving a life won't cost you anything. Go ahead and donate blood.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('images/carsoul3.jpg') }}" class="d-block w-100" height="800px" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <p class="font-white">Once a blood donor is always a lifesaver.</p>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
        <!-- carousel end -->
        <!-- who we are -->

        <section>

            <div class="container">
                <div class="how-section1">
                    <div class="row">
                        <div class="col-md-6 how-img">
                            <img src="{{ asset('images/bannar1.jpg') }}" class=" img-fluid" alt="" />
                        </div>
                        <div class="col-md-6">
                            <h4>Give Blood Save Lifes</h4>
                            <h4 class="subheading">A single drop of blood can make a huge difference.</h4>
                            <p class="text-muted">It is said that blood is one of the most priceless gifts one can give
                                to another. Blood is essential for a person to stay alive. Many times due to accidents
                                or any other serious ailments, a person might require blood. And in those times, people
                                who step up to donate their blood are real-life superheroes</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>
           <!-- need blood -->
           <section>
            <div class="card text-center mb-5">
                <div class="card-body text-black">
                    <h5 class="card-title">Become a Donor?</h5>
                    <p class="card-text">
                        Some quick example text to build on the card title and make up the bulk of the
                        card's content.
                    </p>
                    <a href="{{ route('register') }}" class="btn btn-primary">Join us</a>
                </div>
            </div>
        <section class="mb-5">

            <div class="container" style="margin-top: 0 !important;">
                    <div class="row">
                      <div class="row" style="padding-bottom: 5px;">
                        <img src="{{ asset('images/bannar2.jpg') }}" class=" img-fluid" alt="" />
                    </div>
                        <div class="row" style="color: #ffa500;
                        font-weight: bold;
                        font-size: 30px;">
                            <h4>What Conditions Would Make You Ineligible to Be a Donor?</h4>

                            <p class="text-muted">
                                You will not be eligible to donate blood or platelets if you:
                        </div>
                        <div>
                            <ol>
                                <li> Have tested positive for hepatitis B or hepatitis C, lived with or had sexual
                                    contact in the past 12 months with anyone who has hepatitis B or symptomatic
                                    hepatitis C. </li>
                                <li> Had a tattoo in the past 3 months or received a blood transfusion (except with your
                                    own blood) in the past 3 months. </li>
                                <li> Have ever had a positive test for the AIDS virus. </li>
                                <li>
                                    Have used injectable drugs, including anabolic steroids, unless prescribed by a
                                    physician in the past 3 months.</li>
                                <li>
                                    Blood donors must wait at least 56 days between blood donations and 7 days before
                                    donating platelets. Platelet donors may donate once every seven days, not to exceed
                                    six times in any eight-week period, and must wait 7 days before donating blood.</li>
                            </ol>
                        </div>
                       

                    </div>
                </div>

        </section>

        <!-- need blood -->
        <section class="mb-5">
          <div class="card my-3 mx-4 text-center">
              <div class="card-body text-black">
                  <h5 class="card-title">Need Blood?</h5>
                  <p class="card-text">
                      Some quick example text to build on the card title and make up the bulk of the
                      card's content.
                  </p>
                  <a href="{{ route('bloodreq-user') }}" class="btn btn-primary">Request</a>
              </div>
          </div>
      </section>
      <!-- need blood end-->
        <!-- quotes start -->
        <section class="mb-5">
            <section class="vh-50" style="background-color: #FFE7C7;">
                <div class="container py-5 h-100">
                    <div class="row d-flex align-items-center h-100">
                        <div class="col col-lg-6 mb-4 mb-lg-0">
                            <figure class="text-center bg-white py-5 px-4 shadow-2" style="border-radius: .75rem;">
                                <i class="far fa-gem fa-lg mb-4" style="color: #f9a169;"></i>
                                <blockquote class="blockquote pb-2">
                                    <p>
                                        <i class="fas fa-angle-double-left" style="color: #f9a169;"></i>
                                        <span class="lead font-italic">I have not failed. I've just found 10,000 ways
                                            that won't work.</span>
                                        <i class="fas fa-angle-double-right" style="color: #f9a169;"></i>
                                    </p>
                                </blockquote>
                                <figcaption class="blockquote-footer mb-0 font-italic">
                                    Thomas Edison
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col col-lg-6">
                            <figure class="text-center bg-white py-5 px-4 shadow-2" style="border-radius: .75rem;">
                                <i class="far fa-gem fa-lg mb-4" style="color: #f36f63;"></i>
                                <blockquote class="blockquote pb-2">
                                    <p>
                                        <i class="fas fa-angle-double-left" style="color: #f36f63;"></i>
                                        <span class="lead font-italic">Be less curious about people and more curious
                                            about ideas.</span>
                                        <i class="fas fa-angle-double-right" style="color: #f36f63;"></i>
                                    </p>
                                </blockquote>
                                <figcaption class="blockquote-footer mb-0 font-italic">
                                    Marie Skłodowska Curie
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <!-- quotes end-->



        <div class="container mt-5 mb-5">
    
          <div class="row g-2">
              <div class="col-md-4">
                  <div class="card p-3 text-center px-4">
                      
                      <div class="user-image">
                          
                  <img src="{{asset('images/users/user1.jpg')}}" class="rounded-circle" width="80"
                          >
                          
                      </div>
                      
                      <div class="user-content">
                          
                          <h5 class="mb-0">Bruce Hardy</h5>
                          <span>Software Developer</span>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                          
                      </div>
                      
                      <div class="ratings">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          
                      </div>
                      
                  </div>
              </div>
              
              <div class="col-md-4">
                  
                  <div class="card p-3 text-center px-4">
                      
                      <div class="user-image">
                          
                  <img src="{{asset('images/users/user2.jpg')}}" class="rounded-circle" width="80"
                          >
                          
                      </div>
                      
                      <div class="user-content">
                          
                          <h5 class="mb-0">Mark Smith</h5>
                          <span>Web Developer</span>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                          
                      </div>
                      
                      <div class="ratings">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          
                      </div>
                      
                  </div>
                  
              </div>
              
              <div class="col-md-4">
                  
                  <div class="card p-3 text-center px-4">
                      
                      <div class="user-image">
                          
                  <img src="{{asset('images/users/user3.jpg')}}" class="rounded-circle" width="80"
                          >
                          
                      </div>
                      
                      <div class="user-content">
                          
                          <h5 class="mb-0">Veera  Duncan</h5>
                          <span>Software Architect</span>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                          
                      </div>
                      
                      <div class="ratings">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          
                      </div>
                      
                  </div>
                  
              </div>
              
              
          </div>
          
      </div>



    </main>



</x-frontend.layouts.master>
